| Asset               | Asset Source                                                                             | Asset Author   | Source Author                           |
|---------------------|------------------------------------------------------------------------------------------|----------------|-----------------------------------------|
| Arcanum Tome Sprite | [Arcanum](https://gitlab.com/accensi/hd-addons/arcanum/-/blob/master/Sprites/ARCTZ0.png) | Raven Software | [Accensus](https://gitlab.com/Accensus) |
