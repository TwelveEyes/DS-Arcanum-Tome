### Notes
---
- Casting spells now requires owning an `Arcanum Tome`.
- One minute grace period upon dropping the `Arcanum Tome` until access to magic is lost.
- Allows Flesh and Steel classes to cast spells.
    - F&S class mana regen is slower than full casters.
    - Spells are divided into tiers based on their cost.
    - F&S classes access higher tiers through leveling up.
    - Levels are gained through experience (which is awarded by killing enemies).
    - Fodder enemies don't give experience (e.g. zombies, imps, demons).
